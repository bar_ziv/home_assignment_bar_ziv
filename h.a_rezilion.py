#!/usr/bin/python
# @lint-avoid-python-3-compatibility-imports
#
# Copyright (c) 2015 Brendan Gregg.
# Licensed under the Apache License, Version 2.0 (the "License")

from __future__ import print_function
from bcc import ArgString, BPF
from bcc.utils import printb
import argparse
import ctypes as ct
from datetime import datetime, timedelta
import os
import sys, getopt
import magic

try:
    # check if the arguments are good
    if (sys.argv[1] != "--log_path" or len(sys.argv) <= 2) and (not 'ELF' in magic.from_file(sys.argv[1])):
        print ('''1)    --log_path <log file path>
2)    <ELF file>''')
        sys.exit(2)
# catch wrong index
except:
    print ('''1)    --log_path <log file path>
2)    <ELF file>''')
    sys.exit(2)

processName = ''
filePath = ''
if (len(sys.argv) > 2): # save file path (--log_path)
    filePath = sys.argv[2]
    argType = 1 # sign for 'rezilion' open file
else: # save process name
    processName = sys.argv[1].split('/')[-1]
    argType = 2 # sign for comm tracing

# define BPF program
bpf_text = """
#include <uapi/linux/ptrace.h>
#include <uapi/linux/limits.h>
#include <linux/sched.h>

struct data_t {
    char comm[TASK_COMM_LEN];
    char fname[NAME_MAX];
};
BPF_PERF_OUTPUT(events);

/*
    save comm, filename
*/
int trace_entry(struct pt_regs *ctx, int dfd, const char __user *filename, int flags)
{
    struct data_t data = {};
    
    bpf_get_current_comm(&data.comm, sizeof(data.comm));
    bpf_probe_read(&data.fname, sizeof(data.fname), (void *)filename);
    events.perf_submit(ctx, &data, sizeof(data));
    
    return 0;
};

"""

# initialize BPF
b = BPF(text=bpf_text)
b.attach_kprobe(event="do_sys_open", fn_name="trace_entry")

TASK_COMM_LEN = 16    # linux/sched.h
NAME_MAX = 255        # linux/limits.h

class Data(ct.Structure):
    _fields_ = [
        ("comm", ct.c_char * TASK_COMM_LEN),
        ("fname", ct.c_char * NAME_MAX),
    ]

initialize_ts = 0
check = True # state for writing to the file (write and open/append)

# header
if (argType == 1):
    print("%-30s" %
      ("time"), end="")
    print("PATH")
elif (argType == 2):
    print("%-16s" % ("COMM"))

# process event
def print_event(cpu, data, size):
    event = ct.cast(data, ct.POINTER(Data)).contents
    global check
    global initialize_ts
    
    if (argType == 1):
        fileName = event.fname.split('/')[-1]
        if not (('rezilion' in fileName) and ('.' in fileName)): # check if the file has 'rezilion' in his name and if this is file and not directory
            return -1
        else:
            time = datetime.now()
            print (str(time - initialize_ts) + '    ' +  event.fname)
    elif (argType == 2):
        if not (processName == event.comm): # check if the process is the process that was in the argv
            return -1
        else:
            print (event.comm)

    if(argType == 1):
        if(check):
            directory = os.path.dirname(filePath)
            if not os.path.exists(directory):
                 os.makedirs(directory)
            fileObj = open(filePath, 'w') # write and open new file
            check = False
        else:
            fileObj = open(filePath, 'a') # append new data to the file
        fileObj.write(str(time - initialize_ts) + '  ' + fileName + '\n') # write to the file
        fileObj.close() # close the file
    elif(argType == 2):
        fileObj = open("/tmp/awesome_file.txt", 'w') # open the file
        fileObj.write('Learning never exhausts the mind') # write to the file string
        fileObj.close()

# loop with callback to print_event
b["events"].open_perf_buffer(print_event, page_cnt=64)
initialize_ts = datetime.now()
while 1:
    try:
        b.perf_buffer_poll()
    except KeyboardInterrupt:
        exit()
